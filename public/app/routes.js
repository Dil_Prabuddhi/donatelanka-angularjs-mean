var app = angular.module('appRoutes',['ngRoute'])

.config(function($routeProvider,$locationProvider) {

    $routeProvider

        .when('/', {
            templateUrl: 'app/views/pages/home.html'
        })

        .when('/about', {
            templateUrl: 'app/views/pages/about.html'
        })

        .when('/register', {
            templateUrl: 'app/views/pages/users/register.html',
            controller:'regCtrl',
            controllerAs:'register',
            authenticated: false
        })

        .when('/login', {
            templateUrl: 'app/views/pages/users/login.html',
            authenticated: false
        })

        .when('/logout',{
            templateUrl: 'app/views/pages/users/logout.html',
            authenticated: true
        })

        .when('/profile',{
            templateUrl: 'app/views/pages/users/profile.html',
            authenticated: true
        })

        .when('/authorizer',{
            templateUrl: 'app/views/pages/users/authorizer.html',
            authenticated: true
        })

        .when('/user',{
            templateUrl: 'app/views/pages/users/user.html',
            authenticated: true
        })

        .when('/admin',{
            templateUrl: 'app/views/pages/users/admin.html',
            authenticated: true
        })

        .when('/facebook/:token',{
            templateUrl: 'app/views/pages/users/social/social.html',
            controller: 'facebookCtrl',
            controllerAs: 'facebook',
            authenticated: false
        })

        .when('/facebookerror',{
            templateUrl: 'app/views/pages/users/login.html',
            controller: 'facebookCtrl',
            controllerAs: 'facebook',
            authenticated: false
        })

        .when('/google/:token',{
            templateUrl: 'app/views/pages/users/social/social.html',
            controller: 'googleCtrl',
            controllerAs: 'google',
            authenticated: false
        })

        .when('/googleerror',{
            templateUrl:'app/views/pages/users/login.html',
            controller: 'googleCtrl',
            controllerAs: 'google',
            authenticated: false
        })
        // Route: Google Account-Inactive Error
        .when('/google/inactive/error', {
            templateUrl: 'app/views/pages/users/login.html',
            controller: 'googleCtrl',
            controllerAs: 'google',
            authenticated: false
        })

        // Route: Activate Account Through E-mail
       /* .when('/activate/:token', {
            templateUrl: 'app/views/pages/users/activation/activate.html',
            controller: 'emailCtrl',
            controllerAs: 'email',
            authenticated: false
        })*/

        // Route: Send Username to E-mail
        .when('/resetusername', {
            templateUrl: 'app/views/pages/users/reset/username.html',
            controller: 'usernameCtrl',
            controllerAs: 'username',
            authenticated: false
        })

        // Route: Send Password Reset Link to User's E-mail
        .when('/resetpassword', {
            templateUrl: 'app/views/pages/users/reset/password.html',
            controller: 'passwordCtrl',
            controllerAs: 'password',
            authenticated: false
        })

        // Route: User Enter New Password & Confirm
        .when('/reset/:token', {
            templateUrl: 'app/views/pages/users/reset/newpassword.html',
            controller: 'resetCtrl',
            controllerAs: 'reset',
            authenticated: false
        })

        .otherwise({ redirectTo: '/' });

    $locationProvider.html5Mode({
        enabled:true,
        requireBase:false
    });
});
/*

app.run(['$rootScope','Auth','$location',function ($rootScope,Auth,$location) {

    $rootScope.$on('$routeChangeStart',function(event, next, current){

        if(next.$$route.authenticated==true){
            if(!Auth.isLoggedIn()){
                event.preventDefault();
                $location.path('/');
            }

        }else if(next.$$route.authenticated==false){
            if(Auth.isLoggedIn()){
                event.preventDefault();
                $location.path('/profile');
            }
        }
    });
}]);
*/

// Run a check on each route to see if user is logged in or not (depending on if it is specified in the individual route)
app.run(['$rootScope', 'Auth', '$location', 'User', function($rootScope, Auth, $location, User) {

    // Check each time route changes
    $rootScope.$on('$routeChangeStart', function(event, next, current) {

        // Only perform if user visited a route listed above
        if (next.$$route !== undefined) {
            // Check if authentication is required on route
            if (next.$$route.authenticated === true) {
                // Check if authentication is required, then if permission is required
                if (!Auth.isLoggedIn()) {
                    event.preventDefault(); // If not logged in, prevent accessing route
                    $location.path('/'); // Redirect to home instead
                } else if (next.$$route.permission) {
                    // Function: Get current user's permission to see if authorized on route
                    User.getPermission().then(function(data) {
                        // Check if user's permission matches at least one in the array
                        if (next.$$route.permission[0] !== data.data.permission) {
                            if (next.$$route.permission[1] !== data.data.permission) {
                                event.preventDefault(); // If at least one role does not match, prevent accessing route
                                $location.path('/'); // Redirect to home instead
                            }
                        }
                    });
                }
            } else if (next.$$route.authenticated === false) {
                // If authentication is not required, make sure is not logged in
                if (Auth.isLoggedIn()) {
                    event.preventDefault(); // If user is logged in, prevent accessing route
                    $location.path('/profile'); // Redirect to profile instead
                }
            }
        }
    });
}]);
