angular.module('mainController',['authServices'])

.controller('mainCtrl',function (Auth,$timeout,$location,$rootScope,$window) {
    var app = this;

    app.loadme = false;

    $rootScope.$on('$routeChangeStart',function () {

        if(Auth.isLoggedIn()){
            console.log('Success: User is logged in');
            app.isLoggedIn = true;
            Auth.getUser().then(function (data) {
                app.firstname = data.data.firstname;
                app.lastname = data.data.lastname;
                app.useremail = data.data.email;
                app.username = data.data.username;
                //app.profileimage = data.data.profileimage;
                app.role= data.data.role;
                app.loadme = true;
            });
        }
        else{
            app.isLoggedIn = false;
            app.username='';
            app.loadme = true;
        }
    });

    this.facebook = function () {
        $window.location = $window.location.protocol + '//' + $window.location.host + '/auth/facebook';
    };

    this.google = function () {
        $window.location = $window.location.protocol + '//' + $window.location.host + '/auth/google';
    };

    this.doLogin =function (loginData, regData) {

        app.errorMsg = false;

        Auth.login(app.loginData).then(function (data) {

            if(data.data.success){
                app.successMsg = data.data.message + '...Redirecting';
                Auth.getUser().then(function (data) {

                $timeout(function () {
                    if (data.data.role === 'user') {
                        $location.path('/user');
                    } else if (data.data.role === 'authorizer') {
                        $location.path('/authorizer');
                    } else {
                        $location.path('/admin');
                    }

                    app.loginData={};
                    app.successMsg=false;
                },2000);
                });

            }else{
                app.errorMsg = data.data.message;
            }
        });
    };

    this.logout = function () {
        Auth.logout();
        $location.path('/logout');
        $timeout(function () {
            $location.path('/login');
        },2000);
    };

});


