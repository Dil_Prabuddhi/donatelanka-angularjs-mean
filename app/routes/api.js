var User = require('../models/user');
var jwt = require('jsonwebtoken');
var secret = 'something';
var nodemailer = require('nodemailer');

module.exports = function (router) {



    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'donatelanka01@gmail.com',
            pass: 'passwordDonate123'
        }
    });

    //var client = nodemailer.createTransport(sgTransport(options));


    //user registration
    router.post('/users',function (req,res) {
        var user = new User();
        user.firstname = req.body.firstname;
        user.lastname = req.body.lastname;
        user.email= req.body.email;
        user.username = req.body.username;
        user.password = req.body.password;
        user.role = req.body.role;
        user.temporarytoken = jwt.sign({ username:user.username,username:user.username,email:user.email },secret,{expiresIn:'24h'});

        if(req.body.firstname===null || req.body.firstname===''|| req.body.lastname===null || req.body.lastname==='' ||req.body.email===null || req.body.email==='' || req.body.username===null || req.body.username==='' ||req.body.password===null || req.body.password==='' || req.body.role === null || req.body.role=== ''){
            res.json({success:false,message:'Ensure first name, last name, email, username, password & user role were provided'});
        }else{
            user.save(function (err) {
                if(err){
                    if(err.errors !==null){
                        if (err.errors.firstname) {
                            res.json({ success: false, message: err.errors.firstname.message }); // Display error in validation (firstname)
                        }else if (err.errors.lastname) {
                            res.json({ success: false, message: err.errors.lastname.message }); // Display error in validation (lastname)
                        } else if (err.errors.email) {
                            res.json({ success: false, message: err.errors.email.message }); // Display error in validation (email)
                        } else if (err.errors.username) {
                            res.json({ success: false, message: err.errors.username.message }); // Display error in validation (username)
                        }else if (err.errors.password) {
                            res.json({ success: false, message: err.errors.password.message }); // Display error in validation (password)
                        } else if (err.errors.role) {
                            res.json({ success: false, message: err.errors.role.message }); // Display error in validation (role)
                        }
                        else {
                            res.json({ success: false, message: err }); // Display any other errors with validation
                        }
                    }else if(err){
                        if (err.code == 11000) {
                            if (err.errmsg[61] == "u") {
                                res.json({ success: false, message: 'That username is already taken' }); // Display error if username already taken
                            } else if (err.errmsg[61] == "e") {
                                res.json({ success: false, message: 'That e-mail is already taken' }); // Display error if e-mail already taken
                            }
                        } else {
                            res.json({ success: false, message: err }); // Display any other error
                        }
                    }
                }else{

                   /* // Create e-mail object to send to user
                    var email = {
                        from: 'donatelanka01@gmail.com',
                        to: user.email,
                        subject: 'Your Activation Link',
                        text: 'Hello ' + user.firstname + ', thank you for registering at localhost.com. Please click on the following link to complete your activation: https://localhost:3000/activate/' + user.temporarytoken,
                        html: 'Hello<strong> ' + user.firstname + '</strong>,<br><br>Thank you for registering at localhost.com. Please click on the link below to complete your activation:<br><br><a href="https://localhost:3000/activate/' + user.temporarytoken + '">https://localhost:3000/activate/</a>'
                    };
                    // Function to send e-mail to the user
                    transporter.sendMail(email, function(err, info) {
                        if (err) {
                            console.log(err); // If error with sending e-mail, log to console/terminal
                        } else {
                            console.log('Message sent: ' + info.response);

                        }
                    });*/
                    res.json({success:true, message:'Account registered!'});

                }
            });
        }
    });

   /* // Route to activate the user's account
    router.put('/activate/:token', function(req, res) {
        User.findOne({ temporarytoken: req.params.token }, function(err, user) {
            if (err) {
                // Create an e-mail object that contains the error. Set to automatically send it to myself for troubleshooting.
                var email = {
                    from: 'donatelanka01@gmail.com',
                    to: user.email,
                    subject: 'Error Logged',
                    text: 'The following error has been reported in Donate Lanka: ' + err,
                    html: 'The following error has been reported in Donate Lanka:<br><br>' + err
                };
                // Function to send e-mail to myself
                transporter.sendMail(email, function(err, info) {
                    if (err) {
                        console.log(err); // If error with sending e-mail, log to console/terminal
                    } else {
                        console.log(info); // Log success message to console if sent
                        console.log(user.email); // Display e-mail that it was sent to
                    }
                });
                res.json({ success: false, message: 'Something went wrong. This error has been logged and will be addressed by our staff. We apologize for this inconvenience!' });
            } else {
                var token = req.params.token; // Save the token from URL for verification
                // Function to verify the user's token
                jwt.verify(token, secret, function(err, decoded) {
                    if (err) {
                        res.json({ success: false, message: 'Activation link has expired.' }); // Token is expired
                    } else if (!user) {
                        res.json({ success: false, message: 'Activation link has expired.' }); // Token may be valid but does not match any user in the database
                    } else {
                        user.temporarytoken = false; // Remove temporary token
                        user.active = true; // Change account status to Activated
                        // Mongoose Method to save user into the database
                        user.save(function(err) {
                            if (err) {
                                console.log(err); // If unable to save user, log error info to console/terminal
                            } else {
                                // If save succeeds, create e-mail object
                                var email = {
                                    from: 'donatelanka01@gmail.com',
                                    to: user.email,
                                    subject: 'Account Activated',
                                    text: 'Hello ' + user.firstname + ', Your account has been successfully activated!',
                                    html: 'Hello<strong> ' + user.firstname + '</strong>,<br><br>Your account has been successfully activated!'
                                };
                                // Send e-mail object to user
                                transporter.sendMail(email, function(err, info) {
                                    if (err) {
                                        console.log(err);
                                    }else{
                                        console.log("Message sent:"+info.response);

                                    }
                                });
                                res.json({ success: true, message: 'Account activated!' }); // Return success message to controller
                            }
                        });
                    }
                });
            }
        });
    });*/


    router.post('/checkusername',function (req,res) {
        User.findOne({username:req.body.username}).select('username').exec(function (err,user) {
            if(err) throw err;

            if(user){
                res.json({success:false, message: 'That username is already taken!'});
            }else{
                res.json({success:true, message: 'Valid username'});
            }
        });
    });

    router.post('/checkemail',function (req,res) {
        User.findOne({email:req.body.email}).select('email').exec(function (err,user) {
            if(err) throw err;

            if(user){
                res.json({success:false, message: 'That e-mail is already taken!'});
            }else{
                res.json({success:true, message: 'Valid e-mail'});
            }
        });
    });

    //user login route
    router.post('/authenticate',function (req,res) {
        User.findOne({username:req.body.username}).select('email username password role').exec(function (err,user) {
            if(err) throw err;

            if (!user){
                res.json({success:false,message:'Could not authenticate user'});
            }else if (user){
                if (req.body.password){
                    var validPassword = user.comparePassword(req.body.password);
                    if(!validPassword){
                        res.json({success:false,message:'Could not authenticate password'});
                    }else{
                        var token = jwt.sign({ username:user.username,email:user.email, role:user.role },secret,{expiresIn:'24h'});
                        res.json({success:true,message:'User authenticated',token:token});
                    }
                }else{
                    res.json({success:false,message:'No password provided'});
                }


            }
        });
    });

    router.use(function(req,res,next){
        var token = req.body.token || req.body.query || req.headers['x-access-token'];

        if(token){
            //verify token
            jwt.verify(token,secret,function (err,decoded) {
               if (err){
                   res.json({success:false, message:'Token invalid'});
               }else{
                   req.decoded = decoded;
                   next();
               }
            });
        }else {
            res.json({ success:false, message:'No token provided'});
        }
    });

    router.post('/me',function (req,res) {
        res.send(req.decoded);
    });

    return router;
};