var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var bcrypt   = require('bcrypt-nodejs');
var titlize  = require('mongoose-title-case');
var validate = require('mongoose-validator');

var fnameValidator = [
    validate({
        validator: 'matches',
        arguments: /^[a-zA-Z]{3,20}$/,
        message: 'First Name must be at least 3 characters, max 20, no special characters or numbers.'
    }),
    validate({
        validator: 'isLength',
        arguments: [3, 20],
        message: 'First Name should be between {ARGS[0]} and {ARGS[1]} characters'
    })
];

var lnameValidator = [
    validate({
        validator: 'matches',
        arguments: /^[a-zA-Z]{3,20}$/,
        message: 'Last Name must be at least 3 characters, max 20, no special characters or numbers.'
    }),
    validate({
        validator: 'isLength',
        arguments: [3, 20],
        message: 'Last Name should be between {ARGS[0]} and {ARGS[1]} characters'
    })
];

var emailValidator = [
    validate({
        validator: 'isEmail',
        message: 'Is not a valid e-mail.'
    }),
    validate({
        validator: 'isLength',
        arguments: [3, 25],
        message: 'Email should be between {ARGS[0]} and {ARGS[1]} characters'
    })
];

var usernameValidator = [
    validate({
        validator: 'isLength',
        arguments: [3, 25],
        message: 'Username should be between {ARGS[0]} and {ARGS[1]} characters'
    }),
    validate({
        validator: 'isAlphanumeric',
        message: 'Username must contain letters and numbers only'
    })
];

var passwordValidator = [
    validate({
        validator: 'matches',
        arguments: /^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\d])(?=.*?[\W]).{8,35}$/,
        message: 'Password needs to have at least one lower case, one uppercase, one number, one special character, and must be at least 8 characters but no more than 35.'
    }),
    validate({
        validator: 'isLength',
        arguments: [8, 35],
        message: 'Password should be between {ARGS[0]} and {ARGS[1]} characters'
    })
];

var UserSchema = new Schema({
    firstname: { type: String, required: true, validate:fnameValidator},
    lastname: { type: String, required: true, validate:lnameValidator},
    email: { type: String, required: true, lowercase: true, unique: true,validate:emailValidator},
    username: { type: String, lowercase: true, required: true, unique: true, validate:usernameValidator},
    password: { type: String, required: true,validate:passwordValidator, select:false},
    role: {type: String, required: true},
    active: { type: Boolean, required: true, default: false },
    temporarytoken: { type: String, required: true }
});



UserSchema.pre('save',function (next) {
    var user = this;
    bcrypt.hash(user.password,null,null,function (err,hash) {
        if (err) return next(err);
        user.password = hash;
        next();
    });
});

UserSchema.plugin(titlize, {
    paths: [ 'firstname', 'lastname' ]
});

UserSchema.methods.comparePassword = function (password) {
    return bcrypt.compareSync(password,this.password);
}

module.exports = mongoose.model('User',UserSchema);